import { readFileSync, writeFileSync } from 'fs'
import airportLocator, { Airport } from './src/airport-locator'

const airportData = JSON.parse(readFileSync('alaska_airports.json', 'utf8'))
const airports = airportLocator(airportData as Airport[])

writeFileSync('closest_airports.json', JSON.stringify(airports), 'utf8')
