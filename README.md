# airport locator

This function finds the three closest airports within 500km for each airport listed in `alaska_airports.json`.

Results are output to `closest_airports.json`.

## dependencies

- **node >= 10.0.0**

  It is recommended to install node.js via [nvm](https://github.com/creationix/nvm) using their [install script](https://github.com/creationix/nvm#install-script).

- **yarn >= 1.9.0**

  Yarn [installation instructions](https://yarnpkg.com/en/docs/install#alternatives-tab)

## installation

clone this repository. To install dependencies, run

```
$ yarn
```

in the root directory.

## run script

To run the function and regenerate the result file,

```
$ yarn build
```

## debug

To debug using Chrome's developer tools, run

```
$ yarn build:inspect
```

Then open `about:inspect` in Chrome and click on the _Open dedicated DevTools for Node_ link. The project is rebuilt and reloaded automatically when files are changed when running in this mode.

## test

```
$ yarn test
```

runs test suite.

```
$ yarn test:watch
```

Runs test suite, watching for file changes.
