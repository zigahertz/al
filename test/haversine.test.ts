import chai from 'chai'
import { describe, it } from 'mocha'
import haversine, { EARTH_RADIUS } from '../src/haversine'

const { expect } = chai

describe('haversine formula', () => {
  it('works', () => {
    const northPole = { latitude: 90, longitude: 0 }
    const southPole = { latitude: -90, longitude: 0 }
    const distance = haversine(northPole, southPole)

    // arclength formula:  S = R * theta
    expect(distance).to.equal(EARTH_RADIUS * Math.PI)
  })
})
